<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/img/logo_16.png') }}">
    <title>Apidoc</title>
</head>
<body>
    <div id="app">
        <api-doc></api-doc>
    </div>
</body>
<script>
 var apidoc = @json($apidoc)

 var doc = @json($doc)

 var instances = @json($instances)

 var instancesList = @json($instancesList)

 var configHeaders = @json($configHeaders)
</script>
<script src="/vendor/apidoc/apidoc.js" defer></script>
</html>
