<?php

namespace PkEngine\Apidoc;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ApidocServiceProvider extends ServiceProvider
{

    public function register()
    {

    }

    public function boot()
    {
        Route::middleware(collect(config('apidoc.middlewares'))->toArray())
            ->get('apidoc', 'PkEngine\Apidoc\DocController@index')->name('apidoc');
        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/apidoc'),
        ], ['apidoc-assets', 'laravel-assets']);
        
        $this->publishes([
            __DIR__.'/../config/apidoc.php' => config_path('apidoc.php'),
        ], ['apidoc-config', 'laravel-config']);

        $this->mergeConfigFrom(
            __DIR__.'/../config/apidoc.php', 'apidoc'
        );
        $this->loadViewsFrom(__DIR__.'/../views/', 'apidoc');
    }
}
