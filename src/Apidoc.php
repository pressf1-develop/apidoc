<?php

namespace PkEngine\Apidoc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\DocBlockFactory;

class Apidoc
{
    protected string $path;
    protected string $prefix;
    protected Collection $controllers;
    protected Collection $resources;
    protected Collection $models;

    public function __construct(string $path = '/', string $prefix = '')
    {
        $this->path = $path;
        $this->prefix = $prefix;
        $this->controllers = collect();

    }

    public static function instances()
    {
        $groups = collect(config('apidoc.models', []));
        if($groups->count()){
            return $groups->map(function ($data, $title){
                return [
                    'type' => 'group',
                    'slug' => $title,
                    'title' => $title,
                    'children' => static::pathModels($data['models'], $data['resources'])
                ];
            })->values()->toArray();
        }else{
            return static::pathModels('app/Models', 'app/Http/Resources');
        }
    }

    public static function instancesList()
    {
        $groups = collect(config('apidoc.models', []));
        if($groups->count()){
            return $groups->flatMap(function ($data, $title){
                return static::pathModels($data['models'], $data['resources']);
            });
        }else{
            return static::pathModels('app/Models', 'app/Http/Resources');
        }
    }

    protected static function resources($path)
    {
        return collect(File::allFiles(base_path($path)))->mapWithKeys(function (\Symfony\Component\Finder\SplFileInfo $item){
            return [
                static::prepareInstance(static::getBaseclass($item->getPathname(), 'Resource.php')) =>
                    static::getNamespace($item->getPathname())];
        });
    }

    protected static function recursivePathModels($path, $resourcePath)
    {
        $folders = collect(File::allFiles(base_path($path)))->map(function ($folder) use ($path, $resourcePath){
            $slug = Str::after($folder, $path.'/');
            return [
                'type' => 'group',
                'slug' => $slug,
                'title' => $slug,
                'children' => static::recursivePathModels($path.'/'.$slug, $resourcePath)
            ];
        });
        $resources = static::resources($resourcePath);
        $models = static::pathModels($path, $resourcePath);
        return $folders->merge($models);
    }

    protected static function pathModels($path, $resourcePath)
    {

        $models = collect(File::allFiles(base_path($path)))->mapWithKeys(function (\Symfony\Component\Finder\SplFileInfo $item){
            return [
                static::prepareInstance(static::getBaseclass( $item->getPathname())) =>
                    static::getNamespace($item->getPathname(), 'Models')
            ];
        });
        $resources = static::resources($resourcePath);
        return collect($models)->map(function ($item, $instance) use ($resources){
            return static::prepareModel($item, $instance, $resources);
        })->filter()->values();
    }

    protected static function prepareModel($item, $instance, $resources)
    {
        $ref = new \ReflectionClass($item);
        if($ref->isAbstract() || $ref->isInterface() || $ref->isTrait()){
            return null;
        }
        $model = new $item;
        $resourceClass = $resources->get($instance);
        if(class_exists($resourceClass)){
            $phpDocFactory = DocBlockFactory::createInstance();

            $r = new \ReflectionMethod($resourceClass, 'toArray');
            if(is_string($comment = $r->getDocComment())){
                $phpDoc = $phpDocFactory->create($comment);
                $resource = (new $resourceClass($model))->resolve();
                $description = ($desc = collect($phpDoc->getTagsByName('info'))->first()) ? $desc->getDescription()?->getBodyTemplate() : null;
                $fields = collect($phpDoc->getTagsByName('field'))->map(function ($item){
                    if($descApiDoc = $item->getDescription()){
                        [$field, $description] = explode(':', $descApiDoc->getBodyTemplate(), 2);
                        [$type, $name] = explode(' ', $field, 2);
                        $relation = null;
                        if(Str::startsWith( $type, '#')){
                            $relation = Str::after($type, '#');
                            $type = 'relation';
                        }
                        return [
                            'type' => $type,
                            'name' => $name,
                            'description' => $description,
                            'relation' => $relation,
                            'invalid' => false
                        ];
                    }

                })->filter();
                if($arr = config('apidoc.model_title')){
                    $class = $arr[0];
                    $method = $arr[1];
                    $title = $class::$method($instance, $item);
                }else{
                    $title = __('front.instances.'.Str::camel($instance));
                }
                return [
                    'description' => $description,
                    'title' => $title,
                    'slug' => Str::snake($instance),
                    'type' => 'instance',
                    'fields' => $fields
                ];
            }
        }
    }

    public function generate()
    {
        return collect(\Illuminate\Support\Facades\Route::getRoutes()->getRoutes())->map(function ($item){
            if(Str::startsWith($item->uri, $this->path)){
                [$controller, $action] = explode('@',$item->action['controller']);
                $groupName = Str::betweenFirst($item->action['as'], $this->prefix, '.');
                $r = new \ReflectionMethod('\\'.$controller, $action);
                $phpDocFactory = DocBlockFactory::createInstance();
                $params = [];
                $description = null;
                if(is_string($comment = $r->getDocComment())){
                    $phpDoc = $phpDocFactory->create($comment);
                    $description = ($desc = collect($phpDoc->getTagsByName('info'))->first()) ? $desc->getDescription()?->getBodyTemplate() : null;
                    $params = collect($phpDoc->getTagsByName('apiParam'))->map(function ($item){
                        if($descApiDoc = $item->getDescription()){
                            [$param, $description] = explode(':', $descApiDoc->getBodyTemplate(), 2);
                            [$rules, $name] = explode(' ', $param, 2);
                            $rules = explode('|', $rules);
                            return [
                                'type' => Arr::first($rules),
                                'name' => $name,
                                'description' => $description,
                                'rules' => $rules
                            ];
                        }

                    })->filter()->toArray();

                }
                foreach($r->getParameters() as $parameter){

                    if($parameter->getType() && $name = $parameter->getType()->getName()){
                        if(class_exists($name) && is_subclass_of($name, FormRequest::class)){
                            $excludes = [];
                            foreach ((new $name)->rules() as $field => $rules){

                                if(strripos($field, '.') !== false){
                                    $field = Str::before($field, '.');
                                }
                                if(in_array($field, $excludes)){
                                    continue;
                                }
                                if(is_string($rules)){
                                    $rules = explode('|', $rules);
                                }
                                $rules = collect($rules)->filter(function ($rule){
                                    return is_string($rule) && $rule !== 'bail';
                                });
                                $type = null;
                                if($rules->contains('string')){
                                    $type = 'string';
                                }elseif($rules->contains('integer')){
                                    $type = 'integer';
                                }elseif($rules->contains('array')){
                                    $type = 'array';
                                }
                                $rules->push($type);
                                $rules = $rules->unique();
                                $excludes[] = $field;
                                $params[] = [
                                    'type' => $type,
                                    'name' => $field,
                                    'description' => null,
                                    'rules' => $rules
                                ];
                            }
                        }
                    }

                }
                preg_match_all("/\{([?a-zA-Z_-]*)\}/", $item->uri, $binds);
                $binds = $binds[1];

                return [
                    'binds' => $binds,
                    'name' => $item->action['as'] ?? Str::random(),
                    'description' => $description,
                    'uri' => $item->uri,
                    'url' => url($item->uri),
                    'method' => $item->methods[0],
                    'params' => $params,
                    'group' => $groupName,
                    'query' => [
                        'binds' => collect($binds)->mapWithKeys(function ($value){
                            return [$value => ''];
                        })->toArray(),
                        'params' => collect($params)->mapWithKeys(function ($value){
                            return [$value['name'] => ''];
                        })->toArray()
                    ],
                    'result' => [
                        'status' => null,
                        'text' => '',
                        'json' => ''
                    ]
                ];
            }

        })->filter()->groupBy('group')->map(function ($items, $group){
            $title = $group;
            if($arr = config('apidoc.group_title')){
                $class = $arr[0];
                $method = $arr[1];
                $title = $class::$method($group);
            }else{
                $title = __('front.instances.'.Str::camel($group));
            }
            return [
                'routes' => $items,
                'group' => $group,
                'title' => $title
            ];
        })->values();
    }

    private static function prepareInstance($str)
    {
        $dots = str_replace('\\', '.', $str);
        $str = collect(explode('.', $dots))->map(function ($str){
            if(strtoupper($str) === $str){
                return strtolower($str);
            }else{
                return Str::camel($str);
            }

        })->join('.');
        return Str::camel(str_replace('\\', '.', $str));
    }

    private static function getBaseclass($path, $after = '.php')
    {
        $before = strpos($path, '/') !== false ? '/' : '\\';
        return Str::before(Str::afterLast($path, $before),  $after);
    }

    private static function getNamespace($str)
    {
        $namespace = Str::betweenFirst(File::get($str), 'namespace ', ';').'\\';
        return $namespace.static::getBaseclass( $str);
    }
}
