<?php

namespace PkEngine\Apidoc;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PkEngine\Apidoc\Apidoc;
use Illuminate\Routing\Controller;

class DocController extends Controller
{
    public function index()
    {
        $apidoc = collect(config('apidoc.versions'))->map(function ($prefix, $path){
            return [
                'api' => $path,
                'routes' => (new Apidoc($path, $prefix))->generate(),
            ];
        })->values();
        return view('apidoc::apidoc', [
            'apidoc' => $apidoc,
            'doc' => array_filter([@file_get_contents(base_path('apidoc.md'))]),
            'instances' => Apidoc::instances(),
            'instancesList' => Apidoc::instancesList(),
            'configHeaders' => config('apidoc.headers') ?? new \stdClass()
        ]);
    }
}
