const mix = require('laravel-mix');
if (mix.inProduction()) {
    mix.js('js/apidoc.js', 'public').vue()
}else{
    mix.setPublicPath('./../../../public/vendor/apidoc')
    mix.js('js/apidoc.js', '/').vue()
}

