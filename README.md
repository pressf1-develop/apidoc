
## APIdoc

### Установка

```
composer require --dev pk-engine/apidoc
php artisan vendor:publish --provider="PkEngine\Apidoc\ApidocServiceProvider"
```

```
'providers' => [
  
   PkEngine\Apidoc\ApidocServiceProvider::class,

]
```

### Controller

#### @apiParam тип название: описание

##### Исключительне типы:

- array, object - json-editor

```php
class Controller extends BaseController {

    /**
    * @apiParam string foo: bar
    * 
    * @param Request $request
    * @return void
     */
    public function index(Request $request)
    {
        ///
    }
    
}
```

### Resource

##### @field тип название: описание

##### Исключительне типы:

- \#название - ссылка на ресурс модели


```php
class Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @field #model foo: bar
     * @field int id: ID
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        ////
    }
}
```
