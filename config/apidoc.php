<?php

use Illuminate\Support\Str;

return [

    //[path/url => prefix.name.]
    'versions' => [
        'api/v1' => 'api.v1.'
    ],

    'middleware' => [
        'web'
    ],
    
    //array [Class, 'staticMethod'] prepare, title as arg
    'group_title' => null,

    'headers' => [
        'Authorization' => [
            'placeholder' => 'Bearer token',
            'prefix' => 'Bearer ',
        ]
    ]
];
