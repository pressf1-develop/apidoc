// import { createApp } from '../node_modules/vue'
import { createApp } from '../node_modules/vue/dist/vue.runtime.esm-bundler';
import ApiDoc from './ApiDoc'
// import JsonViewer from 'vue-json-viewer'
window.axios = require('../node_modules/axios');
window._ = require('../node_modules/lodash');
require('../node_modules/bootstrap')
require('../scss/styles.scss')

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.token = document.head.querySelector('meta[name="csrf-token"]');
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

createApp({
    // JsonViewer,
    components: {ApiDoc}
}).mount('#app')
